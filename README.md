Adapted (forked) from the original [passmenu](https://git.zx2c4.com/password-store/tree/contrib/dmenu).

`passmenu` is a [dmenu][]-based interface to [pass][], the standard Unix
password manager. This design allows you to quickly copy a password to the
clipboard without having to open up a terminal window if you don't already have
one open.

`passmenu-with-actions` displays an additional menu letting the user choose an action to execute after having selected a password file.

The supported actions are: Copy to clipboard, Edit, Remove, Rename.

Note: I removed all the "typing" stuff with `xdotool` which I don't use.

# Usage

```sh
passmenu [dmenu arguments...]
```

To use with [i3][] window manager, put in i3 configuration file:

```
set $dmenu_options -i -l 30
bindsym $mod+p exec --no-startup-id EDITOR="gvim -y --nofork" passmenu-with-actions $dmenu_options -p Passwords
```

> `EDITOR` can be replaced with the editor of your choice. It's used for the `Edit` action.

[dmenu]: http://tools.suckless.org/dmenu/
[pass]: http://www.zx2c4.com/projects/password-store/
[id]: https://i3wm.org/
